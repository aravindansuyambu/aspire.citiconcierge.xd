 //JavaScript Document
function enable(){
    var cat = document.getElementById("sel1");
    if(cat.value == "Travel"){
     
           $('#Travel').show(); 
           $('#Dining').hide();
           $('#Event').hide();
           $('#Send_Flower').hide();
           $('#Other_Request').hide();
    }
    if(cat.value == "Dining"){
           $('#Travel').hide(); 
           $('#Dining').show();
           $('#Event').hide();
           $('#Event').hide();
           $('#Event').hide(); 
    }
    if(cat.value == "Event"){
           $('#Travel').hide(); 
           $('#Dining').hide();
           $('#Event').show();
           $('#Send_Flower').hide();
           $('#Other_Request').hide();
       
    }
    if(cat.value == "Send Flower"){
           $('#Travel').hide(); 
           $('#Dining').hide();
           $('#Event').hide();
           $('#Send_Flower').show();
           $('#Other_Request').hide();
       
    }
    if(cat.value == "Other Request"){
           $('#Travel').hide(); 
           $('#Dining').hide();
           $('#Event').hide();
           $('#Send_Flower').hide();
           $('#Other_Request').show();
    }
}


$(document).ready(function (e) {
	
	/*$( ".nav-button-pad" ).hover(
  function() {
    $( ".gly-hover").css( 'color','#ffffff')
  }, function() {
    $(".gly-hover" ).css( 'color','#9d9d9d')
  }
);

$( ".gly-hover" ).hover(
  function() {
    $( ".nav-button-pad").css( 'color','#ffffff')
  }, function() {
    $(".nav-button-pad" ).css( 'color','#9d9d9d')
  }
);
*/
	 
	
	
     $(".content .col-md-9 .col-md-4 h3").click(function (e) {
        var checkElement = $(this).next();
    
        $('.content .col-md-9 .col-md-4').removeClass('active');
        $(this).parent('.col-md-4').addClass('active');    
    
        if((checkElement.is('.holder')) && (checkElement.is(':visible'))) {
            $(this).parent('.col-md-4').removeClass('active');
            checkElement.slideUp('normal');
        }
    
        if((checkElement.is('.holder')) && (!checkElement.is(':visible'))) {
            $('.content .col-md-9 .col-md-4 .holder:visible').slideUp('normal');
            checkElement.slideDown('normal');
        }
    
        if (checkElement.is('.holder')) {
            return false;
        } else {
            return true;    
        }
    });
    
    
     $(".presigncontent .col-md-12 .col-md-3 h3").click(function (e) {
        var checkElement = $(this).next();
    
        $('.presigncontent .col-md-12 .col-md-3').removeClass('active');
        $(this).parent('.col-md-3').addClass('active');    
    
        if((checkElement.is('.holder')) && (checkElement.is(':visible'))) {
            $(this).parent('.col-md-3').removeClass('active');
            checkElement.slideUp('normal');
        }
    
        if((checkElement.is('.holder')) && (!checkElement.is(':visible'))) {
            $('.presigncontent .col-md-12 .col-md-3 .holder:visible').slideUp('normal');
            checkElement.slideDown('normal');
        }
    
        if (checkElement.is('.holder')) {
            return false;
        } else {
            return true;    
        }
    });
    
    
    
    $(".favorites .slider2 h4").click(function (e) {
        
        var checkElement = $(this).next();
        $('.favorites .slider2').removeClass('active');
        $(this).parent('.slider2').addClass('active');    
        
        
        if((checkElement.is('.slider2')) && (checkElement.is(':visible'))) {
            $(this).parent('.checkbox').removeClass('active');
            checkElement.slideUp('normal');
        }
    
        if((checkElement.is('.slider2')) && (!checkElement.is(':visible'))) {
            $('.favorites .checkbox:visible').slideUp('normal');
            checkElement.slideDown('normal');
        }
        
        if (checkElement.is('.checkbox')) {
            return false;
        } else {
            return true;    
        }    
        
    });
    
    
    $(".select-loc").click(function (e) {
         $(".location-holder").slideToggle();        
        });
        

    $("#search1").click(function () {
        $('#search-box').slideDown(300);
        $('#search1').hide();
        $('#search2').show();      
    });
    
    $("#search2").click(function () {      
        $('#search-box').slideUp(300);
        $('#search1').show();
        $('#search2').hide();
    });    
    
    $("#black-up-arrow").click(function () {
        $(this).hide();
        $('#contact-area').slideUp();      
    });    
    
    $("#black-down-arrow").click(function () {
        $(this).hide();
        $("#black-up-arrow").show()
        $('#contact-area').slideDown();
    }); 
    
    
    $("#filter12").hide();
     $(".filter11").hide();
    $("#filter11").click(function () {
        
        $("#filter11").hide();
        $(".filter11").show();
        $("#filter12").show();
    
   });

   
    $("#filter12").click(function () {
        $("#filter12").hide();
        $(".filter11").hide();
        $("#filter11").show();
    });

    
    

    
    

    //stick in the fixed 100% height behind the navbar but don't wrap it
    $('#slide-nav.navbar .container').append($('<div id="navbar-height-col"></div>'));

    // Enter your ids or classes
    var toggler = '.navbar-toggle';
    var pagewrapper = '.small-nav';
    var pagewrapperB = '#dvBanner';
    var navigationwrapper = '.navbar-header';
    var menuwidth = '100%'; // the menu inside the slide menu itself
    var slidewidth = '80%';
    var menuneg = '-100%';
    var slideneg = '-80%';


    $("#slide-nav").on("click", toggler, function (e) {

        var selected = $(this).hasClass('slide-active');
        if(selected)
        {    
            $('.small-banner').css('margin-top','91px')
        }
        else{
            $('.small-banner').css('margin-top','31px')
            };
            
            
        $('#slidemenu').stop().animate({
            left: selected ? menuneg : '0px'
        });

        $('#navbar-height-col').stop().animate({
            left: selected ? slideneg : '0px'
        });
		

        $(pagewrapper).stop().animate({
            left: selected ? '0px' : slidewidth
        });
         
        $(pagewrapperB).stop().animate({
            left: selected ? '0px' : slidewidth
        });
        
        $(navigationwrapper).stop().animate({
            left: selected ? '0px' : slidewidth
        });

        $(this).toggleClass('slide-active', !selected);
        $('#slidemenu').toggleClass('slide-active');
        
        $('#page-content, #dvBanner, .navbar, body, .navbar-header').toggleClass('slide-active');
    });

       
    var selected = '#slidemenu, #page-content, body, .navbar, .navbar-header';
      $(window).on("resize", function () {
        if ($(window).width() > 480 && $('.navbar-toggle').is(':hidden')) {
            $(selected).removeClass('slide-active');
        }
    });

$("#back-to-top").hide();
    //back to top
    $("#back-to-top").click(function(){
        $('body,html').animate({scrollTop:0},1000);
        return false;
    });
    
    
    $(window).scroll(function(){
        if ($(window).scrollTop()>100){
        $("#back-to-top").fadeIn(1500);
        }
        else
        {
        $("#back-to-top").fadeOut(1500);
        }
    });
    
    $('.search-btn').click(function(){
         var search_btn = '.search-btn';
         if($(this).parent('li').hasClass('open'))
            $(search_btn).find('.glyphicon').removeClass('glyphicon-zoom-out').addClass('.glyphicon-search');
         else            
            $(search_btn).find('.glyphicon').removeClass('.glyphicon-search').addClass('glyphicon-zoom-out');
     });    
    $(document).click(function(event) {
       var target = $(event.target);
       var search_btn = '.search-btn';
        if (!target.attr('class').match(/^search-btn/)) {
           $(search_btn).find('.glyphicon').removeClass('glyphicon-zoom-out').addClass('.glyphicon-search');
       }
        else{    
        $(search_btn).find('.glyphicon').removeClass('.glyphicon-search').addClass('glyphicon-zoom-out');
       }
    });
        
});

